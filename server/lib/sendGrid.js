import sgMail from '@sendgrid/mail';
import config from 'config';
sgMail.setApiKey(`${config.sendgrid}`);

export default class MailSender {
	static sendMail(mailOptions) {
		const msg = {
			to: mailOptions.to,
			from: mailOptions.from,
			subject: mailOptions.subject,
			text: mailOptions.text,
		};
		sgMail.send(msg);
	}
}
