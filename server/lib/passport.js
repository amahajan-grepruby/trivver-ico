import passport_local from 'passport-local';
import { UserModel } from '../models';
import config from 'config';

const LocalStrategy = passport_local.Strategy;

export default function (passport) {
  // used to serialize the user for the session
  passport.serializeUser(function (user, done) {
    done(null, user.id);
  });

  // used to deserialize the user
  passport.deserializeUser(function (id, done) {
    UserModel.findById(id)
      .exec(function (err, user) {
        if (user) {
          done(err, user);
        };
      });
  });

  passport.use('local-login', new LocalStrategy({
    usernameField: 'email',
    passwordField: 'password',
    passReqToCallback: true
  }, function (req, email, password, done) {
    UserModel.findOne()
      .where({ email })
      .exec(function (err, user) {
        if (err) {
          return done(err);
        }
        if (!user) {
          return done(null, false, req.flash('errorMessage', 'No user found.'));
        }
        if (!user.validPassword(password)) {
          return done(null, false, req.flash('errorMessage', 'Oops! Wrong password.'));
        }
        return done(null, user);
      });
  }));
};
