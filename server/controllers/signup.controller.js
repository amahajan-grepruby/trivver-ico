import bcrypt from 'bcrypt-nodejs';
import { UserModel } from '../models';

export default class signupController {

	static post(req, res) {
		UserModel.findOne()
		.where({ 'email': req.body.email, is_deleted: false })
		.exec((err, user) => {
			if (user) {
				req.flash('errorMessage', 'User Already exists.')
				return res.redirect('/');
			}
			const userObject = {
				'name': req.body.name,
				'email': req.body.email,
				'password': bcrypt.hashSync(req.body.password, bcrypt.genSaltSync(8), null),
			}
			UserModel.create(userObject, (err, user) => {
				if (err) {
					req.flash('errorMessage', 'Sorry! User Registration failed');
					res.redirect('/');
				}
				if (user) {
					req.login(user, (err) => {
            if (err)
							req.flash('errorMessage', 'Cannot Log in at the moment. Please try again.');
						const redirectUrl = (err) ? '/' : '/dashboard'
						return res.redirect(redirectUrl);
					})
				}
			})
		});
	}
}