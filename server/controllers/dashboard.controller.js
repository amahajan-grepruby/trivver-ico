export default class dashboardController {
  static get(req, res) {
    let user = req.user;
    res.render('dashboard', { user: user });
  }
}