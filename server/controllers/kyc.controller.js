import countries from '../lib/countries.json';
import Request from 'request';
import config from 'config';

export default class kycController {
  static get(req, res) {
    let user = req.user;
    res.render('verifyKyc', { user: user, countries: countries });
  }

  static post(req, res) {
    let user = req.user;
    let params = req.body;
    let authToken = Buffer.from(config['IDMSandboxApiUser']+":"+config['IDMSandboxApiPassword']).toString('base64');
    Request.post({
    	"headers": { "Content-Type": "application/json", "Authorization": "Basic "+authToken},
    	"url": config['IDMSandboxUrl']+"/im/account/consumer",
    	"body": JSON.stringify({
    			man: params.full_name,
          tea: user.email,
          dob: params.dob,
          phn: params.phone_number,
          pm: params.phone_number,
          sco: params.country
    	})
    }, (error, response, body) => {
    	if(error) {
    		return console.dir(error);
      }
      let data = JSON.parse(body);
      user.kyc_status = data.state == 'A' ? 'Accepted' : ''
      user.transaction_id = data.tid
      user.kyc_submited = true
      user.save(function(err) {
        if (!err) {
          return res.redirect('/');
        } else {
          return res.redirect('/');
        }
      });
    });
  }
}