export default class signInController {
  static get(req, res) {
    res.render('signin', { message: req.flash('errorMessage') });
  }

  static post(req, res) {
    res.render('signin');
  }
}