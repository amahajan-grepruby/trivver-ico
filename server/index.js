import express from 'express';
import path from 'path';
import bodyParser from 'body-parser';
import mongoose from 'mongoose';
import cors from 'cors';
import appRoutes from './routes';
import config from 'config';
import morgan from 'morgan';
import connectMongo from 'connect-mongo';
import session from 'express-session';
import cookieParser from 'cookie-parser';
import passport from 'passport';
import flash from 'connect-flash';
import passportFunction from './lib/passport';
// import cron from './cron/cron';

mongoose.connect(`${config.dbUrl}${config.dbName}`);
mongoose.Promise = global.Promise;
const db = mongoose.connection;
db.on('error', console.error.bind(console, 'Mongodb connection error:'));

const app = express();

app.use(morgan('dev'));
app.use(express.static(path.join(__dirname, '/../views')));
app.use(express.static(path.join(__dirname, '/../views/assets')));
app.use(express.static(path.join(__dirname, '/../views/jquery')));
app.use(express.static(path.join(__dirname, '/../views/partials')));

const MongoStore = connectMongo(session);

app.use(bodyParser({limit: '50mb'}));
app.use(bodyParser.urlencoded({ extended: true }));
app.set('view engine', 'ejs');
app.use(cors());
app.use(bodyParser.json());
app.use(bodyParser.raw());
app.use(cookieParser());

app.use(session({
	secret: config.sessionSecret,
	maxAge: new Date(Date.now() + 3600000),
    store: new MongoStore(
            { mongooseConnection: mongoose.connection },
            function(error) {
                if(error) {
                    return console.error('Failed connecting mongostore for storing session data. %s', error.stack);
                }
                return console.log('Connected mongostore for storing session data');
            }
				)}));

app.use(passport.initialize());
app.use(passport.session());
app.use(flash());
passportFunction(passport);
app.use('/' ,appRoutes(passport));

app.listen(3000, () => {
	console.log('Server listening at port 3000');
});