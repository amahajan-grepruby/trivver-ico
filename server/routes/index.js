import _ from 'lodash';
import express from 'express';
import moment from 'moment';
import signin from './signin.routes';
import loginRouter from './login.routes';
import signupRouter from './signup.routes';
import dashboardRouter from './dashboard.routes';
import kycRouter from './kyc.routes';

export default function (passport) {
  const router = express.Router();

  router.route('/')
    .get((req, res) => {
      if(req.user){
        res.redirect('/dashboard');
      } else {
        res.render('index');
      }
    });
  router.use(setLocals);
  router.route('/logout').get(logout);
  router.use('/signin', signin);
  router.use('/signup', signupRouter);
  router.use('/login', loginRouter(passport));
  router.use('/dashboard', loggedIn, dashboardRouter);
  router.use('/verify_kyc', loggedIn, kycRouter);
  return router;
}

function loggedIn(req, res, next) {
  if (req.user) {
    next();
  } else {
    res.redirect('/');
  }
}

function logout(req, res, next) {
  if (req.user) {
    req.logout();
  }
  res.redirect('/');
}

function setLocals(req, res, next) {
  if (req.user) {
    let sessionObject = req.user;
    _.omit(sessionObject, ['password']);
    res.locals = sessionObject;
  }
  next();
}