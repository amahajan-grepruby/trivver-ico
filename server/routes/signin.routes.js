import signInController from '../controllers/signin.controller';
import express from 'express';

const signin = express.Router();

signin.route('/')
	.post(signInController.post)
  .get(signInController.get);

export default signin;
