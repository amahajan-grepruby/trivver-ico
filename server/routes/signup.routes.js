import signupController from '../controllers/signup.controller';
import express from 'express';

const signupRouter = express.Router();

signupRouter.route('/')
  .post(signupController.post);


export default signupRouter;