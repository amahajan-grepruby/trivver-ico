import express from 'express';

export default function(passport) {
  const loginRouter = express.Router();

  loginRouter.route('/')
    .post(function(req, res, next) {
      passport.authenticate('local-login', function(err, user, info) {
        if (err) { return next(err); }
        // Redirect if it fails
        if (!user) { return res.redirect('/signin'); }
        req.logIn(user, function(err) {
          if (err) { return next(err); }
          // Redirect if it succeeds
          return res.redirect('/dashboard');
        });
      })(req, res, next);
    });
  return loginRouter;
}