import kycController from '../controllers/kyc.controller';
import express from 'express';

const dashboard = express.Router();

dashboard.route('/')
  .get(kycController.get)
  .post(kycController.post)

export default dashboard;
