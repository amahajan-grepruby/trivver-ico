import dashboardController from '../controllers/dashboard.controller';
import express from 'express';

const dashboard = express.Router();

dashboard.route('/')
  .get(dashboardController.get)

export default dashboard;
