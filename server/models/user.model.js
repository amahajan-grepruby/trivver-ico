import mongoose from 'mongoose';
const Schema = mongoose.Schema;
import bcrypt from 'bcrypt-nodejs';

const userSchema = new Schema({
  name: {
    type: String,
    required: [true, 'Name is required']
  },
  email: {
    type: String,
    required: [true, 'Email address is required']
  },
  password: {
    type: String,
    required: [true, 'Password is required']
  },
  role: {
    type: String,
    enum: ['Buyer', 'Admin'],
    default: 'Buyer'
  },
  kyc_status: {
    type: String,
    enum: ['Accepted', 'Approved', 'Decliend', 'InReview']
  },
  kyc_submited: {
    type: Boolean,
    default: false
  },
  transaction_id: {
    type: String
  }
}, {
  timestamps: true
});

// methods ======================
// generating a hash
userSchema.methods.generateHash = function(password) {
  return bcrypt.hashSync(password, bcrypt.genSaltSync(8), null);
};

// checking if password is valid
userSchema.methods.validPassword = function(password) {
  return bcrypt.compareSync(password, this.password);
};

const UserModel = mongoose.model('User', userSchema, 'user');
export default UserModel;
